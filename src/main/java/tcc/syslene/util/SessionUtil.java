package tcc.syslene.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class SessionUtil {

	public static Object get(String value){
		return((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true)).getAttribute(value);		 
	}
	
	public static void put(String chave, Object value){
		((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true)).putValue(chave, value);		 
	}
	
	public static void remove(String chave){
		((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true)).removeAttribute(chave);		 
	}
}
