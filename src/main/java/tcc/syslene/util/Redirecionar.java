package tcc.syslene.util;

import java.io.IOException;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

public class Redirecionar {
		
	public static void para(String pagina){		
		try {
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();		
			//String caminhoAplicacao = ec.getApplicationContextPath();
			String caminhoAplicacao = ec.getRequestContextPath();
			ec.redirect(caminhoAplicacao + pagina);
		} catch (IOException e) {
			e.printStackTrace();		
		}
	}
}