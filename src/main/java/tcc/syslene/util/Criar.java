package tcc.syslene.util;

import org.apache.commons.lang3.StringUtils;

public class Criar {
	
	private String sigla;
	private String objeto;
	
	public Criar(String sigla, String objeto) {
		this.sigla = sigla;
		this.objeto = objeto;
	}
			
	public StringBuilder controller(){
		StringBuilder sb = new StringBuilder();
		
		sb.append("package tcc.syslene.controller;");sb.append("\n\r");
		sb.append("import javax.faces.bean.ManagedBean;");sb.append("\n\r");
		sb.append("import javax.faces.bean.ViewScoped;");sb.append("\n\r");
		sb.append("import tcc.syslene.model.Usuario;");sb.append("\n\r");
		sb.append("@ViewScoped");sb.append("\n\r");
		sb.append("@ManagedBean");sb.append("\n\r");
		sb.append("public class "+ StringUtils.capitalize(objeto.toLowerCase()) +"Controller extends GenericController<"+ StringUtils.capitalize(objeto.toLowerCase()) +", Integer> {");sb.append("\n\r");
		sb.append("			");sb.append("\n\r");
		sb.append("	public "+ StringUtils.capitalize(objeto.toLowerCase()) +"Controller() {");sb.append("\n\r");
		sb.append("		super();");sb.append("\n\r");
		sb.append("	}");sb.append("\n\r");
		sb.append("	");sb.append("\n\r");
		sb.append("	@Override");sb.append("\n\r");
		sb.append("	public void newInstance() {");sb.append("\n\r");
		sb.append("		super.obj = new "+ StringUtils.capitalize(objeto.toLowerCase()) +"();");sb.append("\n\r");
		sb.append("	}");sb.append("\n\r");
		sb.append("	");sb.append("\n\r");
		sb.append("	@Override");sb.append("\n\r");
		sb.append("	public boolean valido() {");sb.append("\n\r");
		sb.append("		return (("+ StringUtils.capitalize(objeto.toLowerCase()) +")obj).get"+ StringUtils.capitalize(sigla.toLowerCase()) +"ncodg() == null;");sb.append("\n\r");
		sb.append("	}");sb.append("\n\r");
		sb.append("}");
		
		return sb;
	}
	
	public StringBuilder xhtml(){
		StringBuilder sb = new StringBuilder();		
		sb.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>");sb.append("\n\r");
		sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"");sb.append("\n\r");
		sb.append("	xmlns:h=\"http://java.sun.com/jsf/html\"");sb.append("\n\r");
		sb.append("	xmlns:f=\"http://java.sun.com/jsf/core\"");sb.append("\n\r");
		sb.append("	xmlns:ui=\"http://java.sun.com/jsf/facelets\"");sb.append("\n\r");
		sb.append("	xmlns:p=\"http://primefaces.org/ui\">");sb.append("\n\r");
		sb.append("<ui:composition template=\"/login.xhtml\">	");sb.append("\n\r");
		sb.append("	<ui:define name=\"content\">");sb.append("\n\r");
		sb.append("	");sb.append("\n\r");
		sb.append("		<h:form id=\"f"+sigla.toLowerCase()+"\">");sb.append("\n\r");
		sb.append("		<p:messages globalOnly=\"true\" closable=\"true\"/>");sb.append("\n\r");
		sb.append("			<br></br>	");sb.append("\n\r");
		sb.append("		<h:panelGrid columns=\"3\" columnClasses=\"ui-grid-col-2, ui-grid-col-8, ui-grid-col-2\" layout=\"grid\">");sb.append("\n\r");
		sb.append("			");sb.append("\n\r");
		sb.append("			<p:spacer>");sb.append("\n\r");
		sb.append("			</p:spacer>");sb.append("\n\r");
		sb.append("			");sb.append("\n\r");
		sb.append("			<h:column>");sb.append("\n\r");
		sb.append("			<h:panelGrid columns=\"2\">");sb.append("\n\r");
		sb.append("			");sb.append("\n\r");
		sb.append("				<p:outputLabel value=\"Descrição:\"/>");sb.append("\n\r");
		sb.append("        		<p:inputText value=\"#{"+ objeto +"Controller.obj."+sigla.toLowerCase()+"cdesc}\" />");sb.append("\n\r");
		sb.append("			");sb.append("\n\r");
		sb.append("				<p:commandButton value=\"Salvar\" actionListener=\"#{"+objeto+"Controller.cadastrar()}\" update=\"f"+sigla.toLowerCase()+"\"/>");sb.append("\n\r");
		sb.append("			</h:panelGrid>");sb.append("\n\r");
		sb.append("			<br></br>");sb.append("\n\r");
		sb.append("			");sb.append("\n\r");
		sb.append("			<p:contextMenu for=\""+sigla.toLowerCase()+"list\">");sb.append("\n\r");
		sb.append("		        <p:menuitem value=\"Editar\" actionListener=\"#{"+objeto+"Controller.clickEditar()}\" update=\":f"+sigla.toLowerCase()+"\"/>");sb.append("\n\r");
		sb.append("		        <p:menuitem value=\"Excluir\" actionListener=\"#{"+objeto+"Controller.deletar()}\" update=\":f"+sigla.toLowerCase()+"\">");sb.append("\n\r");
		sb.append("		        	<p:confirm header=\"Confirmação\" message=\"Deseja excluir?\" icon=\"ui-icon-alert\" />    ");sb.append("\n\r");
		sb.append("		        </p:menuitem>");sb.append("\n\r");
		sb.append("		    </p:contextMenu>");sb.append("\n\r");
		sb.append("		    ");sb.append("\n\r");
		sb.append("		    <p:confirmDialog global=\"true\" showEffect=\"fade\" hideEffect=\"fade\">");sb.append("\n\r");
		sb.append("		        <p:commandButton value=\"Sim\" type=\"button\" styleClass=\"ui-confirmdialog-yes\" icon=\"ui-icon-check\" />");sb.append("\n\r");
		sb.append("		        <p:commandButton value=\"Não\" type=\"button\" styleClass=\"ui-confirmdialog-no\" icon=\"ui-icon-close\" />");sb.append("\n\r");
		sb.append("		    </p:confirmDialog>");sb.append("\n\r");
		sb.append("	 ");sb.append("\n\r");
		sb.append("			<p:dataTable id=\""+sigla.toLowerCase()+"list\" value=\"#{"+objeto+"Controller.list}\" var=\""+sigla.toLowerCase()+"\" rowKey=\"#{"+sigla.toLowerCase()+"."+sigla.toLowerCase()+"ncodg}\"");sb.append("\n\r");
		sb.append("	                 selection=\"#{"+objeto+"Controller.obj_}\" selectionMode=\"single\">");sb.append("\n\r");
		sb.append("					<p:column headerText=\"Descrição\">");sb.append("\n\r");
		sb.append("						<h:outputText value=\"#{"+sigla.toLowerCase()+"."+sigla.toLowerCase()+"cdesc}\" />");sb.append("\n\r");
		sb.append("					</p:column>								");		sb.append("\n\r");
		sb.append("			</p:dataTable>");sb.append("\n\r");
		sb.append("					");sb.append("\n\r");
		sb.append("			</h:column>	");sb.append("\n\r");
		sb.append("			");sb.append("\n\r");
		sb.append("			<p:spacer>");sb.append("\n\r");
		sb.append("			</p:spacer>");sb.append("\n\r");
		sb.append("								");sb.append("\n\r");
		sb.append("	</h:panelGrid>		");sb.append("\n\r");
		sb.append("		");sb.append("\n\r");
		sb.append("	</h:form>");sb.append("\n\r");
		sb.append("	");sb.append("\n\r");
		sb.append("	</ui:define>					");sb.append("\n\r");
		sb.append("</ui:composition>");sb.append("\n\r");
		sb.append("</html>");
		return sb;
	}

	public static void main(String[] args) {
		Criar c = new Criar("lc", "localidade");
		//System.out.println((c.controller()).toString());
		System.out.println("\n\r\n\r" + (c.xhtml()).toString());
	}
}
