package tcc.syslene.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tcc.syslene.model.Localidade;

public class JsonReader {

  private static String readAll(Reader rd) throws IOException {
    StringBuilder sb = new StringBuilder();
    int cp;
    while ((cp = rd.read()) != -1) {
      sb.append((char) cp);
    }
    return sb.toString();
  }

  public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
    InputStream is = new URL(url).openStream();
    try {
      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
      String jsonText = readAll(rd);
      JSONObject json = new JSONObject(jsonText);
      return json;
    } finally {
      is.close();
    }
  }

  public static void main(String[] args) throws IOException, JSONException {
	  /*
    JSONObject json = readJsonFromUrl("http://localhost:8080/syslene/rest/auth/");
    //System.out.println(json.toString());
    JSONArray array = json.getJSONArray("localidade");
    
    Iterator<Object> iterator = array.iterator();
	while (iterator.hasNext()) {
		JSONObject obj = (JSONObject)iterator.next();
		
		Localidade lc = new Localidade();
		lc.setLcncodg(Integer.parseInt((String)obj.get("lcncodg")));
		lc.setLccdesc((String)obj.get("lccdesc"));
		
		System.out.println(lc);		
	}*/
	  	 
  }
}