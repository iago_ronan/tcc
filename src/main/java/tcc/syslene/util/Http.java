package tcc.syslene.util;

import java.util.List;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import tcc.syslene.dao.GenericDAO;
import tcc.syslene.dao.JPAUtil;
import tcc.syslene.model.Domicilio;
import tcc.syslene.model.Usuario;

public class Http {

	private static final String USER_AGENT = "Mozilla/5.0";
	
	public static void sendGet(){}
		
	public static void sendPost(){
		Client client = new Client().create();
		WebResource wr = client.resource("http://localhost:8080/syslene/rest/");
		String strPost = "<usuario>"						
						+ "<usclogn>"
						+ "admin"
						+ "</usclogn>"
						+ "<uscpasw>"
						+ "admin"
						+ "</uscpasw>"						
						+ "<usciddp>"
						+ "9999"
						+ "</usciddp>"
						+ "</usuario>";						
		/*
		String strPost = "<tipousuarioes>"
				+ "<tipousuario>"						
				+ "<tucdesc>"
				+ "Testando"
				+ "</tucdesc>"
				+ "</tipousuario>"
				+ "<tipousuario>"						
				+ "<tucdesc>"
				+ "Testando1"
				+ "</tucdesc>"
				+ "</tipousuario>"
				+ "<tipousuario>"						
				+ "<tucdesc>"
				+ "Testando2"
				+ "</tucdesc>"
				+ "</tipousuario>"
				+"</tipousuarioes>";
		*/
		ClientResponse cr = wr.path("auth")
				.queryParam("token", "#syslene")				
				.type("text/xml").post(ClientResponse.class, strPost);
		
		 if (cr.getStatus() != 201) {
			 throw new RuntimeException("Failed : HTTP error code : "+ cr.getStatus());
	     }

        System.out.println("Output from Server .... \n");
        String output = cr.getEntity(String.class);
        System.out.println(output);
	}
	
	public static void sendPost2(){
		Client client = new Client().create();
		WebResource wr = client.resource("http://localhost:8080/syslene/rest/");
		String strPost = getJsonDomicilio();						
		
		GenericDAO<Usuario, Integer> dao = new GenericDAO<Usuario, Integer>(JPAUtil.getEntityManager());
		Usuario us = dao.find(Usuario.class, 1); 
		
		ClientResponse cr = wr.path("auth")
				.queryParam("token", "#syslene")
				.queryParam("usclogn", us.getUsclogn())
				.queryParam("uscpasw", us.getUscpasw())
				.queryParam("usciddp", us.getUsciddp())										
				.type("text/xml").post(ClientResponse.class, strPost);
		
		 if (cr.getStatus() != 201) {
			 throw new RuntimeException("Failed : HTTP error code : "+ cr.getStatus());
	     }

        System.out.println("Output from Server .... \n");
        String output = cr.getEntity(String.class);
        System.out.println(output);
	}
	
	private static String getJsonDomicilio(){
		GenericDAO<Domicilio, Integer> dao = new GenericDAO<Domicilio, Integer>(JPAUtil.getEntityManager());
		List<Domicilio> ldm = dao.findWithNamedQuery("Domicilio.findAll", null);				
		
		StringBuilder sb = new StringBuilder();
		sb.append("<domicilioes>");
		sb.append("<domicilio>");
		sb.append("<dmcendr>rua 01, n 50</dmcendr>");
		sb.append("<dmcmord>iago</dmcmord>");
		sb.append("<dmlagua>false</dmlagua>");
		sb.append("<dmlcist>false</dmlcist>");
		sb.append("<dmlesgt>false</dmlesgt>");
		sb.append("<dmlfilt>false</dmlfilt>");
		sb.append("<dmlpia>false</dmlpia>");
		sb.append("<dmlpoco>true</dmlpoco>");
		sb.append("<dmlreci>false</dmlreci>");
		sb.append("<dmlres1>false</dmlres1>");
		sb.append("<dmlresv>true</dmlresv>");
		sb.append("<dmlreus>false</dmlreus>");
		sb.append("<dmlsani>false</dmlsani>");
		sb.append("<dmlsumi>false</dmlsumi>");
		sb.append("<dmltan1>false</dmltan1>");
		sb.append("<dmltanq>false</dmltanq>");
		sb.append("<dmlvala>false</dmlvala>");
		sb.append("<dmncglc>");		
		sb.append("<lcncodg>4</lcncodg>");
		sb.append("</dmncglc>");		
		sb.append("</domicilio>");
		sb.append("</domicilioes>");
		/*
		sb.append("<domicilioes>");
		
		for (Domicilio dm : ldm) {			
			sb.append("<domicilio>");
			sb.append("<dmcendr>" + dm.getDmcendr()+ " </dmcendr>");
			sb.append("<dmcmord>" + dm.getDmcmord()+ " </dmcmord>");
			sb.append("<dmlagua>" + dm.getDmlagua()+ " </dmlagua>");
			sb.append("<dmlcist>" + dm.getDmlcist()+ " </dmlcist>");
			sb.append("<dmlesgt>" + dm.getDmlesgt()+ " </dmlesgt>");
			sb.append("<dmlfilt>" + dm.getDmlfilt()+ " </dmlfilt>");
			sb.append("<dmlpia> " + dm.getDmlpia()+ "  </dmlpia>");
			sb.append("<dmlpoco>" + dm.getDmlpoco()+ " </dmlpoco>");
			sb.append("<dmlreci>" + dm.getDmlreci()+ " </dmlreci>");
			sb.append("<dmlres1>" + dm.getDmlres1()+ " </dmlres1>");
			sb.append("<dmlresv>" + dm.getDmlresv()+ " </dmlresv>");
			sb.append("<dmlreus>" + dm.getDmlreus()+ " </dmlreus>");
			sb.append("<dmlsani>" + dm.getDmlsani()+ " </dmlsani>");
			sb.append("<dmlsumi>" + dm.getDmlsumi()+ " </dmlsumi>");
			sb.append("<dmltan1>" + dm.getDmltan1()+ " </dmltan1>");
			sb.append("<dmltanq>" + dm.getDmltanq()+ " </dmltanq>");
			sb.append("<dmlvala>" + dm.getDmlvala()+ " </dmlvala>");
			sb.append("<dmncglc>");		
			sb.append("<lcncodg>" + dm.getDmncglc().getLcncodg() + "</lcncodg>");
			sb.append("</dmncglc>");						
			sb.append("</domicilio>");
		}		
		sb.append("</domicilioes>");
		*/	
		
		/*
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		
		for (Domicilio dm : ldm) {
			JSONObject jsonObject_ = new JSONObject();
			jsonObject_.put("dmncodg", dm.getDmncodg());
			jsonObject_.put("dmncglc", dm.getDmncglc().getLcncodg());
			jsonObject_.put("dmncgus", dm.getDmncgus().getUsncodg());
			jsonObject_.put("dmcmord", dm.getDmcmord());
			jsonObject_.put("dmcendr", dm.getDmcendr());
			jsonObject_.put("dmclat", dm.getDmclat());
			jsonObject_.put("dmclong", dm.getDmclong());
			jsonObject_.put("dmnhabt", dm.getDmnhabt());
			jsonObject_.put("dmlagua", dm.getDmlagua());
			jsonObject_.put("dmlpoco", dm.getDmlpoco());
			jsonObject_.put("dmlcist", dm.getDmlcist());
			jsonObject_.put("dmlresv", dm.getDmlresv());
			jsonObject_.put("dmlres1", dm.getDmlres1());
			jsonObject_.put("dmlsani", dm.getDmlsani());
			jsonObject_.put("dmlpia", dm.getDmlpia());
			jsonObject_.put("dmltanq", dm.getDmltanq());
			jsonObject_.put("dmlfilt", dm.getDmlfilt());
			jsonObject_.put("dmltan1", dm.getDmltan1());
			jsonObject_.put("dmlsumi", dm.getDmlsumi());
			jsonObject_.put("dmlvala", dm.getDmlvala());
			jsonObject_.put("dmlreus", dm.getDmlreus());
			jsonObject_.put("dmlesgt", dm.getDmlesgt());
			jsonObject_.put("dmlreci", dm.getDmlreci());
			jsonObject_.put("dmdcadt", dm.getDmdcadt());
			jsonObject_.put("dmdaltr", dm.getDmdaltr());
						
			jsonArray.add(jsonObject_);
		}
		//jsonObject.put("domicilio", jsonArray);
		return jsonArray.toJSONString();
		*/
		return sb.toString();
	}
	
	public static void main(String[] args) {
		//new Http().sendPost();
		new Http().sendPost2();
	}

}
