package tcc.syslene.util;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class Message {

	public static void setMensagem(Severity severity, String msg){
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, msg, null));							
	}
	
	public static void sucesso(String msg){
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null));							
	}
	
	public static void erro(String msg){
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null));							
	}
	
	public static void erro(String id, String msg){
		FacesContext.getCurrentInstance().addMessage(id, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null));							
	}
	
	public static void warn(String msg){
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, msg, null));							
	}
	
	public static void erroFatal(String msg){
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, msg, null));							
	}
}
