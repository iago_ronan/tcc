package tcc.syslene.filter;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

import tcc.syslene.controller.AutenticacaoController;
import tcc.syslene.util.Redirecionar;

public class AutenticacaoFilter implements PhaseListener {

	@Override
	public void afterPhase(PhaseEvent event) {	
		FacesContext fc = event.getFacesContext();			
		
        if (fc.getViewRoot().getViewId().contains("/pages/")) {        	
            try {

            	HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);            	
            	if (session == null) {
            		System.out.println("Sessão Nula");
            		Redirecionar.para("/");
            	}
            	
            	AutenticacaoController controller = (AutenticacaoController) session.getAttribute("autenticacaoController");            	
                if (controller.getUs() != null && controller.getUs().getUsncodg() != null) {                	                
                	fc.getApplication().getNavigationHandler().handleNavigation(fc, null, "/index.xhtml");                	
                }else{
                	System.out.println("Usuário não autenticado");
                	Redirecionar.para("/");
                }
            } catch (Exception e) {                                              
            	Redirecionar.para("/");
            }

        }
	}

	@Override
	public void beforePhase(PhaseEvent arg0) {}

	@Override
	public PhaseId getPhaseId() {		
		return PhaseId.RESTORE_VIEW;
	}

}