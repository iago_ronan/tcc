package tcc.syslene.controller;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tcc.syslene.model.Parametro;
@ViewScoped
@ManagedBean
public class ParametroController extends GenericController<Parametro, Integer> {
			
	public ParametroController() {
		super();
		super.obj = list.get(0);
	}
	
	@Override
	public void newInstance() {
		super.obj = new Parametro();
	}
	
	@Override
	public boolean valido() {
		return ((Parametro)obj).getPancodg() == null;
	}
}
