package tcc.syslene.controller;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import tcc.syslene.dao.GenericDAO;
import tcc.syslene.dao.JPAUtil;
import tcc.syslene.model.Usuario;
import tcc.syslene.util.Criptografar;
import tcc.syslene.util.Message;
import tcc.syslene.util.Redirecionar;

@SessionScoped
@ManagedBean
public class AutenticacaoController{
	
	private Usuario us;		
	private StreamedContent file;
     
	public AutenticacaoController() {
		us = new Usuario();
		download();
	}
	
	public void download() {        
        InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/download/Syslene.apk");
        file = new DefaultStreamedContent(stream, "application/vnd.android.package-archive", "syslene_mobile.apk");
    }
 
    public StreamedContent getFile() {
        return file;
    }

					
	public boolean isPaginaIndex(){
		String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
		return viewId.contains("index");
	}
	
	public void login(){		
		GenericDAO<Usuario, Integer> dao = null;

		Map<String, Object> params = new HashMap<String, Object>();		
		
		params.put("usclogn", us.getUsclogn());
		params.put("uscpasw", Criptografar.MD5(us.getUsclogn(), us.getUscpasw()));
		try{
			dao = new GenericDAO<Usuario, Integer>(JPAUtil.getEntityManager());		
		}catch(Exception e){
			Message.erro("Erro instancia dao.");
		}
		
		Usuario us_ = null;
		try{			
			us_ = (Usuario) dao.findWithNamedQuerySingle("Usuario.findByUsclognUscpasw", params);
			if (us_ != null) {
				us = us_;			
				Redirecionar.para("/pages/index.xhtml");
			}
		}catch(Exception e){
			Message.erro("Autenticação negada.");
		}finally{			
			dao.close();
		}								
	}
	
	public void logout(){
		HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		session.removeAttribute("autenticacaoController");
		session.invalidate();
		Redirecionar.para("/");
	}

	public Usuario getUs() {
		return us;
	}

	public void setUs(Usuario us) {
		this.us = us;
	}

		
}
