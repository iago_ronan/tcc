package tcc.syslene.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tcc.syslene.model.Tipousuario;
import tcc.syslene.util.Redirecionar;

@ViewScoped
@ManagedBean
public class TipousuarioController extends GenericController<Tipousuario, Integer> {
			
	public TipousuarioController() {
		super();
	}		
	
	@Override
	public void newInstance() {
		super.obj = new Tipousuario();
	}
	
	@Override
	public boolean valido() {
		return ((Tipousuario)obj).getTuncodg() == null;
	}	
	
	public void redirecionar(){
		Redirecionar.para("/pages/usuario.xhtml");
	}
}
