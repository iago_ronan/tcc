package tcc.syslene.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import tcc.syslene.dao.GenericDAO;
import tcc.syslene.dao.JPAUtil;
import tcc.syslene.model.Domicilio;
import tcc.syslene.util.Message;
import tcc.syslene.util.SessionUtil;

@ViewScoped
@ManagedBean
public class MapaController {

	private MapModel mapa;
	private List<Domicilio> ldm;
	
	public MapaController() {
		ldm = (List<Domicilio>) SessionUtil.get("mapear");
		if (ldm == null) {			
			GenericDAO<Domicilio, Integer> dmdao = new GenericDAO<Domicilio, Integer>(JPAUtil.getEntityManager());
			ldm = dmdao.findWithNamedQuery("Domicilio.findAll", null);
			dmdao.close();
		}else{
			SessionUtil.remove("mapear");
		}
		
		mapa = new DefaultMapModel();		
		for (Domicilio dm : ldm) {
			if (dm.getDmclat() != null && !dm.getDmclat().isEmpty() && dm.getDmclong() != null && !dm.getDmclong().isEmpty()) {				
				mapa.addOverlay(new Marker(new LatLng(Double.parseDouble(dm.getDmclat()), Double.parseDouble(dm.getDmclong())),
						dm.getDmcendr() + " - " + dm.getDmcmord()));
			}
		}
	}
	
	public void mostrarDomicilio(OverlaySelectEvent event){
		Marker m = (Marker) event.getOverlay();
		Message.sucesso(m.getTitle());
	}

	public MapModel getMapa() {
		return mapa;
	}

	public void setMapa(MapModel mapa) {
		this.mapa = mapa;
	}
	
	
}
