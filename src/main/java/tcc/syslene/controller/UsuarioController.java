package tcc.syslene.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tcc.syslene.dao.GenericDAO;
import tcc.syslene.dao.JPAUtil;
import tcc.syslene.model.Tipousuario;
import tcc.syslene.model.Usuario;
import tcc.syslene.util.Criptografar;

@ViewScoped
@ManagedBean
public class UsuarioController extends GenericController<Usuario, Integer> {
			
	public UsuarioController() {
		super();
	}
	
	public void novo(){
		editar = false;
		newInstance();
	}
	
	@Override
	public void newInstance() {
		super.obj = new Usuario();
	}
	
	@Override
	public boolean valido() {
		return ((Usuario)obj).getUsncodg() == null;
	}		
	
	@Override
	public void antesDeCadastrarOuEditar() {
		criptografar();
		GenericDAO<Tipousuario, Integer> dao = new GenericDAO<Tipousuario, Integer>(JPAUtil.getEntityManager());
		obj.setUsncgtu(dao.find(Tipousuario.class, obj.getUsncgtu().getTuncodg()));
	}
		
	public void criptografar() {
		obj.setUscpasw(Criptografar.MD5(obj.getUsclogn(), obj.getUscpasw()));
		obj.setUsciddp(Criptografar.MD5(obj.getUsclogn(), obj.getUsciddp()));
	}
}
