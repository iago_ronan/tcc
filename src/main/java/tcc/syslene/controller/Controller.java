package tcc.syslene.controller;

public interface Controller {

	public void cadastrar();
	public void editar();
	public void deletar();
	public void buscar();
}
