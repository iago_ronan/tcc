package tcc.syslene.controller;

import java.util.List;

import tcc.syslene.dao.GenericDAO;
import tcc.syslene.dao.JPAUtil;
import tcc.syslene.util.Criptografar;
import tcc.syslene.util.Message;

public class GenericController<T, ID> implements Controller {

	protected GenericDAO<T, ID> dao;
	protected T obj;
	protected T obj_;
	protected List<T> list;
	protected List<T> list_;
	protected boolean editar;
	
	public GenericController() {
		editar = false;
		newInstance();
		dao = new GenericDAO<T, ID>(JPAUtil.getEntityManager());		
		list = dao.findWithNamedQuery(obj.getClass().getSimpleName() + ".findAll", null);
		//list_ = new ArrayList<T>();
		dao.close();
	}
	
	public void clickEditar(){
		editar = true;
		obj = obj_;
	}
	
	public boolean isEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}
	
	public T getObj() {
		return obj;
	}

	public void setObj(T obj) {
		this.obj = obj;
	}
		
	public T getObj_() {
		return obj_;
	}

	public void setObj_(T obj_) {
		this.obj_ = obj_;
	}	

	public List<T> getList_() {
		return list_;
	}

	public void setList_(List<T> list_) {
		this.list_ = list_;
	}

	@Override
	public void cadastrar() {
		try {
			if (obj != null && !editar && valido()) {
				antesDeCadastrarOuEditar();
				dao = new GenericDAO<T, ID>(JPAUtil.getEntityManager());
				dao.insert(obj);
				dao.close();
				list.add(obj);
				newInstance();
				Message.sucesso("Registro cadastrado com sucesso.");
			}else{
				editar();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
		

	@Override
	public void editar() {
		try {
			if (obj != null && editar) {
				antesDeCadastrarOuEditar();
				dao = new GenericDAO<T, ID>(JPAUtil.getEntityManager());
				dao.update(obj);
				dao.close();
				newInstance();
				Message.sucesso("Registro editado com sucesso.");
				//editar = false;
			}		
		}catch(Exception e){
			e.printStackTrace();
		}		
	}
	
	@Override
	public void deletar() {
		try {			
			if (obj_ != null && valido()) {
				dao = new GenericDAO<T, ID>(JPAUtil.getEntityManager());				
				dao.remove(dao.getEm().contains(obj_) ? obj_ : dao.getEm().merge(obj_));
				dao.close();
				list.remove(obj_);
				newInstance();
				Message.sucesso("Registro deletado com sucesso.");				
			}		
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void newInstance(){}
	public void criptografar() {}
	public void antesDeCadastrarOuEditar() {}
	public boolean valido(){return true;}
	
	@Override
	public void buscar() {}
		
}

