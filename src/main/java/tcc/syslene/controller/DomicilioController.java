package tcc.syslene.controller;

import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tcc.syslene.model.Domicilio;
import tcc.syslene.search.Cisterna;
import tcc.syslene.search.ConjuntoSanitario;
import tcc.syslene.search.FiltroDomestico;
import tcc.syslene.search.ISearch;
import tcc.syslene.search.LevantamentoPendente;
import tcc.syslene.search.LigacaoDomiciliarDeAgua;
import tcc.syslene.search.LigacaoDomiciliarDeEsgoto;
import tcc.syslene.search.PiaDeCozinha;
import tcc.syslene.search.PocoRaso;
import tcc.syslene.search.RecipienteDeResiduosSolidos;
import tcc.syslene.search.ReservatorioElevado;
import tcc.syslene.search.ReservatorioSemielevado;
import tcc.syslene.search.SistemaDeReuso;
import tcc.syslene.search.Sumidouro;
import tcc.syslene.search.TanqueDeLavarRoupas;
import tcc.syslene.search.TanqueSeptico;
import tcc.syslene.search.ValaDeInfiltracao;
import tcc.syslene.util.Message;
import tcc.syslene.util.Redirecionar;
import tcc.syslene.util.SessionUtil;

@ViewScoped
@ManagedBean
public class DomicilioController extends GenericController<Domicilio, Integer> {

	//ATRIBUTOS DA CLASSE
	private List<Domicilio> ldm;
	private Domicilio dm;
	private boolean listaDeBackup;

	// CONSTRUTOR
	public DomicilioController() {
		super();
		dm = new Domicilio();
		listaDeBackup = false;
	}				
			
	// REMOVE FILTROS DE BUSCA
	public void removerFiltroDaLista() {
		
		if (ldm != null && !ldm.isEmpty()) {			
			super.list = ldm;
		}
		
		listaDeBackup = false;
		desmarcarTodos();
	}

	// ABRE PAGINA DE MAPA COM FILTRO DE DOMICILIOS
	public void mapaDeDomicilios() {
		if (super.list_ != null) {
			if (!super.list_.isEmpty()){				
				SessionUtil.put("mapear", super.list_);
				Redirecionar.para("/pages/mapa.xhtml");
			}
		} else if (super.list != null) {
			if (!super.list.isEmpty()){				
				SessionUtil.put("mapear", super.list);
				Redirecionar.para("/pages/mapa.xhtml");
			}
		} else
			Message.erro("Lista de Domicílios está vazia!");
	}

	// FILTRA LISTA DE DOMICILIO COM OS FILTROS SELECIONADOS
	public void buscar() {
		
		if (listaDeBackup)
			super.list = ldm;
		else {
			ldm = super.list;
			listaDeBackup = true;
		}

		List<ISearch> lis = instanciarFiltroDeBusca();
		
		if (list_ != null && !list_.isEmpty()) {
			for (ISearch is : lis) {
				if (is != null)
					super.list_ = is.buscarNecessita(super.list_);
			}
		}else {			
			for (ISearch is : lis) {
				if (is != null)
					super.list = is.buscarNecessita(super.list);
			}
		}
		
	}
	
	public void buscarPendentes(){
		if (listaDeBackup)
			super.list = ldm;
		else {
			ldm = super.list;
			listaDeBackup = true;
		}
		
		ISearch is = new LevantamentoPendente();
		if (list_ != null && !list_.isEmpty()){			
			is.buscarNecessita(list_);
		}else{
			is.buscarNecessita(list);
		}
	}

	// INSTANCIA UMA LISTA COM FILTROS SELECIONADOS
	private List<ISearch> instanciarFiltroDeBusca() {
		return Arrays.asList(dm.getDmlagua() ? new LigacaoDomiciliarDeAgua() : null,
				dm.getDmlpoco() ? new PocoRaso() : null, dm.getDmlcist() ? new Cisterna() : null,
				dm.getDmlresv() ? new ReservatorioElevado() : null,
				dm.getDmlres1() ? new ReservatorioSemielevado() : null,
				dm.getDmlsani() ? new ConjuntoSanitario() : null, dm.getDmlpia() ? new PiaDeCozinha() : null,
				dm.getDmltanq() ? new TanqueDeLavarRoupas() : null, dm.getDmlfilt() ? new FiltroDomestico() : null,
				dm.getDmltan1() ? new TanqueSeptico() : null, dm.getDmlsumi() ? new Sumidouro() : null,
				dm.getDmlvala() ? new ValaDeInfiltracao() : null, dm.getDmlreus() ? new SistemaDeReuso() : null,
				dm.getDmlesgt() ? new LigacaoDomiciliarDeEsgoto() : null,
				dm.getDmlreci() ? new RecipienteDeResiduosSolidos() : null);
	}

	// CRIA NOVA INSTANCIA DO OBJETO COMO DOMICILIO
	@Override
	public void newInstance() {
		super.obj = new Domicilio();
	}

	// CHECA SE O DOMICILIO POSSUI CODIGO
	@Override
	public boolean valido() {
		return ((Domicilio) obj).getDmncodg() == null;
	}

	// MARCA TODOS OS FILTROS DE BUSCA
	public void marcarTodos() {
		dm.setDmlagua(true);
		dm.setDmlpoco(true);
		dm.setDmlcist(true);
		dm.setDmlresv(true);
		dm.setDmlres1(true);
		dm.setDmlsani(true);
		dm.setDmlpia(true);
		dm.setDmltanq(true);
		dm.setDmlfilt(true);
		dm.setDmltan1(true);
		dm.setDmlsumi(true);
		dm.setDmlvala(true);
		dm.setDmlreus(true);
		dm.setDmlesgt(true);
		dm.setDmlreci(true);
	}

	// DESMARCA TODOS OS FILTROS DE BUSCA
	public void desmarcarTodos() {
		dm.setDmlagua(false);
		dm.setDmlpoco(false);
		dm.setDmlcist(false);
		dm.setDmlresv(false);
		dm.setDmlres1(false);
		dm.setDmlsani(false);
		dm.setDmlpia(false);
		dm.setDmltanq(false);
		dm.setDmlfilt(false);
		dm.setDmltan1(false);
		dm.setDmlsumi(false);
		dm.setDmlvala(false);
		dm.setDmlreus(false);
		dm.setDmlesgt(false);
		dm.setDmlreci(false);
	}

	//GETTERS AND SETTERS
	public Domicilio getDm() {
		return dm;
	}

	public void setDm(Domicilio dm) {
		this.dm = dm;
	}	
	
}
