package tcc.syslene.controller;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tcc.syslene.dao.GenericDAO;
import tcc.syslene.dao.JPAUtil;
import tcc.syslene.model.Localidade;
import tcc.syslene.model.Usuario;
@ViewScoped
@ManagedBean
public class LocalidadeController extends GenericController<Localidade, Integer> {
			
	public LocalidadeController() {
		super();
	}
	
	@Override
	public void newInstance() {
		super.obj = new Localidade();
	}
	
	@Override
	public void antesDeCadastrarOuEditar() {		
		if(editar){
			GenericDAO<Usuario, Integer> dao = new GenericDAO<Usuario, Integer>(JPAUtil.getEntityManager());
			obj.setLcncgus(dao.find(Usuario.class, obj.getLcncgus().getUsncodg())); //seta usuario
			obj.setLcdaltr(new Date());	//data de alteracao
		}
	}
	
	@Override
	public boolean valido() {
		return ((Localidade)obj).getLcncodg() == null;
	}
}
