package tcc.syslene.search;

import java.util.List;

import tcc.syslene.model.Domicilio;

public interface ISearch {

	public List<Domicilio> buscarNecessita(List<Domicilio> ldm);
	//public List<Domicilio> buscarPossue(List<Domicilio> ldm);
		
}
