package tcc.syslene.search;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import tcc.syslene.model.Domicilio;

public class LigacaoDomiciliarDeEsgoto implements ISearch {


	@Override
	public List<Domicilio> buscarNecessita(List<Domicilio> ldm) {
		List<Domicilio> lista_aux = new ArrayList<Domicilio>();
		
		for (Iterator<Domicilio> it = ldm.iterator(); it.hasNext();) {
			Domicilio dm =  it.next();
			if (dm.getDmlesgt())
				lista_aux.add(dm);
		}	
		
		return lista_aux;
	}

	

}
