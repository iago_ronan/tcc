package tcc.syslene.dao;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

public class GenericDAO <T, ID> {
	
	private T t;
	private EntityManager em;	
	
	public GenericDAO(EntityManager em){
		this.em = em;			
	}
	
	
	public EntityManager getEm() {
		return em;
	}


	public void setEm(EntityManager em) {
		this.em = em;
	}


	public T getT() {
		return t;
	}


	public void setT(T t) {
		this.t = t;
	}


	public T insert(T t){		
		
		check();
		EntityTransaction et = em.getTransaction();
		
		et.begin();
		em.persist(t);
		et.commit();
		
		/*
		try{
			et.begin();
			em.persist(t);
			et.commit();
			
		}catch(Exception e){			
			e.printStackTrace();
			if(et.isActive())
				et.rollback();
		}finally{				
			if(et.isActive())
				et.rollback();
		}
		*/			
		
		return t;
	}
	
	public T update(T t){		
		
		check();
		EntityTransaction et = em.getTransaction();
		
		et.begin();
		em.merge(t);			
		et.commit();
		
		/*try{
			et.begin();
			em.merge(t);			
			et.commit();
		}catch(Exception e){
			e.printStackTrace();
			if(et.isActive())
				et.rollback();
		}finally{					
			if(et.isActive())
				et.rollback();
		}*/							
		return t;
	}
	
	public T find(Class<T> t,ID id){	
		
		check();
		return em.find(t, id);
	}
	
	public boolean remove(T t){		
		
		check();
		EntityTransaction et = em.getTransaction();
		
		et.begin();
		em.remove(t);
		et.commit();
		
		/*
		try{
			et.begin();
			em.remove(t);
			et.commit();			
		}catch(Exception e){
			e.printStackTrace();
			if(et.isActive())
				et.rollback();
			return false;
		}finally{
			if(et.isActive())
				et.rollback();			
		}*/
		
		return true;
	}
	
	public boolean remove(Class<T> t,ID id){		
		
		check();
		
		EntityTransaction et = em.getTransaction();				
		
		et.begin();
		em.remove(em.getReference(t, id));
		et.commit();
		
		/*
		try{
			et.begin();
			em.remove(em.getReference(t, id));
			et.commit();
		}catch(Exception e){
			if(et.isActive())
				et.rollback();
			return false;
		}finally{
			if(et.isActive())
				et.rollback();
		}*/					
		return true;
	}		
	
	private void check(){
		if (!em.isOpen()) {
			em = JPAUtil.getEntityManager();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findWithNamedQuery(String hsql, Map<String, Object> params) {
		
		check();
		
		Query query = em.createNamedQuery(hsql);
		if (params != null) {
			for (String i : params.keySet()) {
				query.setParameter(i, params.get(i));
			}
		}		
		
		return query.getResultList();				
	}
	
	@SuppressWarnings("unchecked")
	public T findWithNamedQuerySingle(String hsql, Map<String, Object> params) {
				
		check();
		Query query = em.createNamedQuery(hsql);		
		if (params != null) {
			for (String i : params.keySet()) {
				query.setParameter(i, params.get(i));
			}
		}		
		
		return (T) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findWithCreateQuery(String hsql, Map<String, Object> params) {
		check();
		List<T> util = null;	
		try{
			Query query = em.createQuery(hsql);
			if (params != null) {
				for (String i : params.keySet()) {
					query.setParameter(i, params.get(i));
				}
			}
			
			util = query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}					
		return util;
	}
	
	@SuppressWarnings("unchecked")
	public T findWithCreateQuerySingle(String hsql, Map<String, Object> params) {
		check();
		T util = null;	
		try{
			Query query = em.createQuery(hsql);
			query.setMaxResults(1);
			if (params != null) {
				for (String i : params.keySet()) {
					query.setParameter(i, params.get(i));
				}
			}
			
			util = (T) query.getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}					
		return util;
	}
	
	public void close(){
		if(em.isOpen()){
			em.close();			
		}
		if(em != null){
			em = null;
		}
	}
}
