package tcc.syslene.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
	
	private static final EntityManagerFactory factory = buildEmf();

	private static EntityManagerFactory buildEmf() {		
		try {					
			BancoProperties properties = new BancoProperties("&*%SAJF(C<MMNAH%!KJKDJAUS&A�!(*KOkja76342oikadfafi-0843-");
			return Persistence.createEntityManagerFactory("projetosyslene", properties.getProperties());											
		} catch (Throwable e) {
			System.out.println("Erro ao criar o objeto inicial EntityManagerFactory. Erro: " +e);
			throw new ExceptionInInitializerError(e);
		}		
	}
	
	public static EntityManager getEntityManager(){
		return factory.createEntityManager();
	}		
}