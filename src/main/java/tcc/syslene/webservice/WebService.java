package tcc.syslene.webservice;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import tcc.syslene.dao.GenericDAO;
import tcc.syslene.dao.JPAUtil;
import tcc.syslene.model.Domicilio;
import tcc.syslene.model.Localidade;
import tcc.syslene.model.Usuario;

@Path("/auth")
public class WebService {

	private String token = "syslene";
	
	@GET
	@Produces("application/json")
	public List<Localidade> sincronizar(){
		GenericDAO<Localidade, Integer> dao = new GenericDAO<Localidade, Integer>(JPAUtil.getEntityManager());				
		return dao.findWithNamedQuery("Localidade.findAll", null);
	}
	
	@POST
	@Consumes("application/json")
	@Produces("text/plain")
	public Response sinc(List<Localidade> llc, @QueryParam("token") String token, 
			@QueryParam("usclogn") String usclogn, @QueryParam("uscpasw") String uscpasw, @QueryParam("usciddp") String usciddp){
		
		String result = "";
		int status = 0;
		Usuario us = new Usuario();
		us.setUsclogn(usclogn);
		us.setUscpasw(uscpasw);
		us.setUsciddp(usciddp);
		
		if (token.equals(this.token) && llc != null && !llc.isEmpty()) {			
			Usuario us_ = autenticar(us);
			if (us_ != null && us_.getUsncodg() != null) {
				GenericDAO<Localidade, Integer> dao = new GenericDAO<Localidade, Integer>(JPAUtil.getEntityManager());
				for (Localidade lc : llc) {
					lc.setLcdaltr(new Date());
					lc.setLcncgus(us_);
					dao.update(lc);
				}
				status = 201;
				result = "Sincronização Realizada Com Sucesso!";
			}
		}		
		
		return Response.status(status).entity(result).build();
	}
	
	/*
	@POST
	@Consumes("application/json")
	@Produces("text/plain")
	public Response sincronizarLevantamentos(List<Domicilio> ldm, @QueryParam("token") String token, 
			@QueryParam("usclogn") String usclogn, @QueryParam("uscpasw") String uscpasw, @QueryParam("usciddp") String usciddp){
		
		//List<Domicilio> ldm = new ArrayList<Domicilio>();
		
		String result = "";
		int status = 0;
		Usuario us = new Usuario();
		us.setUsclogn(usclogn);
		us.setUscpasw(uscpasw);
		us.setUsciddp(usciddp);
		
		if (token.equals(this.token) && ldm != null && !ldm.isEmpty()) {			
			Usuario us_ = autenticar(us);
			if (us_ != null && us_.getUsncodg() != null) {
				GenericDAO<Domicilio, Integer> dao = new GenericDAO<Domicilio, Integer>(JPAUtil.getEntityManager());
				for (Domicilio dm : ldm) {
					dm.getDmncgus().setUsncodg(us_.getUsncodg());
					dm.setDmdcadt(new Date());
					dm.setDmncodg(null);
					dao.insert(dm);
				}
				status = 201;
				result = "Sincronização Realizada Com Sucesso!";
			}
		}		
		
		return Response.status(status).entity(result).build();
	}*/
	
	private Usuario autenticar(Usuario us){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("usclogn", us.getUsclogn());
		params.put("uscpasw", us.getUscpasw());
		params.put("usciddp", us.getUsciddp());			

		GenericDAO<Usuario, Integer> dao = new GenericDAO<Usuario, Integer>(JPAUtil.getEntityManager());		
		return dao.findWithNamedQuerySingle("Usuario.findByUsclognUscpaswUsciddp", params);
	}	
	
	/*
	@POST	
	@Consumes("text/xml")
	@Produces("text/plain")
	public Response cadastrarTipousuario(List<Tipousuario> ltu, 
			@QueryParam("login") String login, @QueryParam("senha") String senha,
			@QueryParam("idDispositivo") String idDispositivo, @QueryParam("token") String token){
		
		int status = 999;
		int qtd = 0;
		int size = ltu.size(); 
		String result = "";
		
		// CHECAR ESTAS INFORMAÇÕES NO BANCO
		if ("123".equals(login) && "456".equals(senha) && 
				"123abc".equals(idDispositivo) && "iago".equals(token)) {			
			
			GenericDAO<Tipousuario, Integer> dao = new GenericDAO<Tipousuario, Integer>(JPAUtil.getEntityManager());
			for (int i = 0; i < size; i++) {
				dao.insert(ltu.get(i));				
				qtd++;
			}
			dao.close();
			
			result = "TIPOUSUARIO [" + qtd + " de " + size + "]";
			status = 201;
		}
		return Response.status(status).entity(result).build();
	}
	@GET
	@Produces("text/xml")
	public List<Domicilio> sincronizar(){
		GenericDAO<Domicilio, Integer> dao = new GenericDAO<Domicilio, Integer>(JPAUtil.getEntityManager());				
		return dao.findWithNamedQuery("Domicilio.findAll", null);
	}
	@GET
	@Produces("application/json")
	public List<Localidade> sincronizar(){
		GenericDAO<Localidade, Integer> dao = new GenericDAO<Localidade, Integer>(JPAUtil.getEntityManager());				
		return dao.findWithNamedQuery("Localidade.findAll", null);
	}
	@GET
	@Produces("application/json")
	public List<Domicilio> sincronizar(){
		GenericDAO<Domicilio, Integer> dao = new GenericDAO<Domicilio, Integer>(JPAUtil.getEntityManager());				
		return dao.findWithNamedQuery("Domicilio.findAll", null);
	}
	*/
	
	/*
	@POST
	@Consumes("text/xml")
	@Produces("text/plain")
	public Response autenticar(Usuario us, @QueryParam("token") String token){
		
		String result = "";
		int status = 0;
		
		if (this.token.equals(token)) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("usclogn", us.getUsclogn());
			params.put("uscpasw", us.getUscpasw());
			params.put("usciddp", us.getUsciddp());			

			GenericDAO<Usuario, Integer> dao = new GenericDAO<Usuario, Integer>(JPAUtil.getEntityManager());
			if (dao.findWithNamedQuerySingle("Usuario.findByUsclognUscpaswUsciddp", params) != null) {
				status = 201;
				result = "Usuário Autenticado!";
			}
		}										
		return Response.status(status).entity(result).build();
	}	
	*/
	
}
