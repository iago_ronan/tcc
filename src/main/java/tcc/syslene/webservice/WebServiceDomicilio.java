package tcc.syslene.webservice;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import tcc.syslene.dao.GenericDAO;
import tcc.syslene.dao.JPAUtil;
import tcc.syslene.model.Domicilio;
import tcc.syslene.model.Usuario;

@Path("/domicilio")
public class WebServiceDomicilio {

	private String token = "syslene";
	
	@POST
	@Consumes("application/json")
	@Produces("text/plain")
	public Response sincronizarLevantamentos(List<Domicilio> ldm, @QueryParam("token") String token, 
			@QueryParam("usclogn") String usclogn, @QueryParam("uscpasw") String uscpasw, @QueryParam("usciddp") String usciddp){
		
		//List<Domicilio> ldm = new ArrayList<Domicilio>();
		
		String result = "";		
		int status = 0;
		Usuario us = new Usuario();
		us.setUsclogn(usclogn);
		us.setUscpasw(uscpasw);
		us.setUsciddp(usciddp);
		
		if (token.equals(this.token) && ldm != null && !ldm.isEmpty()) {			
			Usuario us_ = autenticar(us);
			if (us_ != null && us_.getUsncodg() != null) {
				GenericDAO<Domicilio, Integer> dao = new GenericDAO<Domicilio, Integer>(JPAUtil.getEntityManager());
				for (Domicilio dm : ldm) {
					dm.getDmncgus().setUsncodg(us_.getUsncodg());
					dm.setDmdlevt(dm.getDmdcadt()); // data do levantamento
					dm.setDmdcadt(new Date());	// data de cadastro
					dm.setDmncodg(null);
					dao.insert(dm);
				}
				status = 201;
				result = "Sincronização Realizada Com Sucesso!";
			}
		}		
		
		return Response.status(status).entity(result).build();
	}
	
	private Usuario autenticar(Usuario us){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("usclogn", us.getUsclogn());
		params.put("uscpasw", us.getUscpasw());
		params.put("usciddp", us.getUsciddp());			

		GenericDAO<Usuario, Integer> dao = new GenericDAO<Usuario, Integer>(JPAUtil.getEntityManager());		
		return dao.findWithNamedQuerySingle("Usuario.findByUsclognUscpaswUsciddp", params);
	}
}
