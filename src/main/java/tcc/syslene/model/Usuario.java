/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc.syslene.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author iago
 */
@Entity
@Table(name = "usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u"),
    @NamedQuery(name = "Usuario.findByUsncodg", query = "SELECT u FROM Usuario u WHERE u.usncodg = :usncodg"),
    @NamedQuery(name = "Usuario.findByUscdesc", query = "SELECT u FROM Usuario u WHERE u.uscdesc = :uscdesc"),
    @NamedQuery(name = "Usuario.findByUscmail", query = "SELECT u FROM Usuario u WHERE u.uscmail = :uscmail"),
    @NamedQuery(name = "Usuario.findByUsclogn", query = "SELECT u FROM Usuario u WHERE u.usclogn = :usclogn"),
    @NamedQuery(name = "Usuario.findByUscpasw", query = "SELECT u FROM Usuario u WHERE u.uscpasw = :uscpasw"),
    @NamedQuery(name = "Usuario.findByUsclognUscpaswUsciddp", query = "SELECT u FROM Usuario u WHERE u.uscpasw = :uscpasw and u.usclogn = :usclogn and u.usciddp = :usciddp"),
    @NamedQuery(name = "Usuario.findByUsclognUscpasw", query = "SELECT u FROM Usuario u WHERE u.uscpasw = :uscpasw and u.usclogn = :usclogn and u.usncgtu.tuncodg = 1"),
    @NamedQuery(name = "Usuario.findByUsciddp", query = "SELECT u FROM Usuario u WHERE u.usciddp = :usciddp")})
public class Usuario implements Serializable {
    @OneToMany(mappedBy = "dmncgus")
    private List<Domicilio> domicilioList;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "usncodg")
    private Integer usncodg;
    @Column(name = "uscdesc")
    private String uscdesc;
    @Column(name = "uscmail")
    private String uscmail;
    @Column(name = "usclogn")
    private String usclogn;
    @Column(name = "uscpasw")
    private String uscpasw;
    @Column(name = "usciddp")
    private String usciddp;
    @JoinColumn(name = "usncgtu", referencedColumnName = "tuncodg")
    @ManyToOne
    private Tipousuario usncgtu;

    public Usuario() {
    	usncgtu = new Tipousuario();
    }

    public Usuario(Integer usncodg) {
        this.usncodg = usncodg;
        usncgtu = new Tipousuario();
    }

    public Integer getUsncodg() {
        return usncodg;
    }

    public void setUsncodg(Integer usncodg) {
        this.usncodg = usncodg;
    }

    public String getUscdesc() {
        return uscdesc;
    }

    public void setUscdesc(String uscdesc) {
        this.uscdesc = uscdesc;
    }

    public String getUscmail() {
        return uscmail;
    }

    public void setUscmail(String uscmail) {
        this.uscmail = uscmail;
    }

    public String getUsclogn() {
        return usclogn;
    }

    public void setUsclogn(String usclogn) {
        this.usclogn = usclogn;
    }

    public String getUscpasw() {
        return uscpasw;
    }

    public void setUscpasw(String uscpasw) {
        this.uscpasw = uscpasw;
    }

    public String getUsciddp() {
        return usciddp;
    }

    public void setUsciddp(String usciddp) {
        this.usciddp = usciddp;
    }

    public Tipousuario getUsncgtu() {
        return usncgtu;
    }

    public void setUsncgtu(Tipousuario usncgtu) {
        this.usncgtu = usncgtu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usncodg != null ? usncodg.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.usncodg == null && other.usncodg != null) || (this.usncodg != null && !this.usncodg.equals(other.usncodg))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "syslene.Usuario[ usncodg=" + usncodg + " ]";
    }

    @XmlTransient
    public List<Domicilio> getDomicilioList() {
        return domicilioList;
    }

    public void setDomicilioList(List<Domicilio> domicilioList) {
        this.domicilioList = domicilioList;
    }
    
}