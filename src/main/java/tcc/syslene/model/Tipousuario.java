/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc.syslene.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author iago
 */
@Entity
@Table(name = "tipousuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipousuario.findAll", query = "SELECT t FROM Tipousuario t"),
    @NamedQuery(name = "Tipousuario.findByTuncodg", query = "SELECT t FROM Tipousuario t WHERE t.tuncodg = :tuncodg"),
    @NamedQuery(name = "Tipousuario.findByTucdesc", query = "SELECT t FROM Tipousuario t WHERE t.tucdesc = :tucdesc")})
public class Tipousuario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tuncodg")
    private Integer tuncodg;
    @Column(name = "tucdesc")
    private String tucdesc;
    @OneToMany(mappedBy = "usncgtu")
    private List<Usuario> usuarioList;

    public Tipousuario() {
    }

    public Tipousuario(Integer tuncodg) {
        this.tuncodg = tuncodg;
    }

    public Integer getTuncodg() {
        return tuncodg;
    }

    public void setTuncodg(Integer tuncodg) {
        this.tuncodg = tuncodg;
    }

    public String getTucdesc() {
        return tucdesc;
    }

    public void setTucdesc(String tucdesc) {
        this.tucdesc = tucdesc;
    }

    @XmlTransient
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tuncodg != null ? tuncodg.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipousuario)) {
            return false;
        }
        Tipousuario other = (Tipousuario) object;
        if ((this.tuncodg == null && other.tuncodg != null) || (this.tuncodg != null && !this.tuncodg.equals(other.tuncodg))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "syslene.Tipousuario[ tuncodg=" + tuncodg + " ]";
    }
    
}
