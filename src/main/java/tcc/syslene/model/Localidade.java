/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc.syslene.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author iago
 */
@Entity
@Table(name = "localidade")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Localidade.findAll", query = "SELECT l FROM Localidade l"),
		@NamedQuery(name = "Localidade.findByLcncodg", query = "SELECT l FROM Localidade l WHERE l.lcncodg = :lcncodg"),
		@NamedQuery(name = "Localidade.findByLccdesc", query = "SELECT l FROM Localidade l WHERE l.lccdesc = :lccdesc") })
public class Localidade implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "LCNCODG")
	private Integer lcncodg;
	@Column(name = "LCCDESC")
	private String lccdesc;
	@Column(name = "LCLAGUA", columnDefinition = "BIT", length = 1)
	private Boolean lclagua;
	@Column(name = "LCLESGT", columnDefinition = "BIT", length = 1)
	private Boolean lclesgt;
	@Column(name = "LCLCOLT", columnDefinition = "BIT", length = 1)
	private Boolean lclcolt;
	@Column(name = "LCDALTR")
    @Temporal(TemporalType.TIMESTAMP)
	private Date lcdaltr;
	@JoinColumn(name = "LCNCGUS", referencedColumnName = "usncodg")
    @ManyToOne
    private Usuario lcncgus;

	public Localidade() {
		lcncgus = new Usuario();
	}

	
	
	public Usuario getLcncgus() {
		return lcncgus;
	}



	public void setLcncgus(Usuario lcncgus) {
		this.lcncgus = lcncgus;
	}



	public Localidade(Integer lcncodg) {
		this.lcncodg = lcncodg;
	}

	public Boolean getLclagua() {
		return lclagua;
	}

	public void setLclagua(Boolean lclagua) {
		this.lclagua = lclagua;
	}

	public Boolean getLclesgt() {
		return lclesgt;
	}

	public void setLclesgt(Boolean lclesgt) {
		this.lclesgt = lclesgt;
	}

	public Boolean getLclcolt() {
		return lclcolt;
	}

	public void setLclcolt(Boolean lclcolt) {
		this.lclcolt = lclcolt;
	}

	public Date getLcdaltr() {
		return lcdaltr;
	}

	public void setLcdaltr(Date lcdaltr) {
		this.lcdaltr = lcdaltr;
	}

	public Integer getLcncodg() {
		return lcncodg;
	}

	public void setLcncodg(Integer lcncodg) {
		this.lcncodg = lcncodg;
	}

	public String getLccdesc() {
		return lccdesc;
	}

	public void setLccdesc(String lccdesc) {
		this.lccdesc = lccdesc;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (lcncodg != null ? lcncodg.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Localidade)) {
			return false;
		}
		Localidade other = (Localidade) object;
		if ((this.lcncodg == null && other.lcncodg != null)
				|| (this.lcncodg != null && !this.lcncodg.equals(other.lcncodg))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "syslene.Localidade[ lcncodg=" + lcncodg + ", lccdesc=" + lccdesc + " ]";
	}

}
