/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc.syslene.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author iago
 */
@Entity
@Table(name = "parametro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Parametro.findAll", query = "SELECT p FROM Parametro p"),
    @NamedQuery(name = "Parametro.findByPancodg", query = "SELECT p FROM Parametro p WHERE p.pancodg = :pancodg"),
    @NamedQuery(name = "Parametro.findByPacuf", query = "SELECT p FROM Parametro p WHERE p.pacuf = :pacuf"),
    @NamedQuery(name = "Parametro.findByPacmuni", query = "SELECT p FROM Parametro p WHERE p.pacmuni = :pacmuni")})
public class Parametro implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PANCODG")
    private Integer pancodg;
    @Column(name = "PACUF")
    private String pacuf;
    @Column(name = "PACMUNI")
    private String pacmuni;
    

    public Parametro() {
    }

    public Parametro(Integer pancodg) {
        this.pancodg = pancodg;
    }

    public Integer getPancodg() {
        return pancodg;
    }

    public void setPancodg(Integer pancodg) {
        this.pancodg = pancodg;
    }

    public String getPacuf() {
        return pacuf;
    }

    public void setPacuf(String pacuf) {
        this.pacuf = pacuf;
    }

    public String getPacmuni() {
        return pacmuni;
    }

    public void setPacmuni(String pacmuni) {
        this.pacmuni = pacmuni;
    }

    
    

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (pancodg != null ? pancodg.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parametro)) {
            return false;
        }
        Parametro other = (Parametro) object;
        if ((this.pancodg == null && other.pancodg != null) || (this.pancodg != null && !this.pancodg.equals(other.pancodg))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "syslene.Parametro[ pancodg=" + pancodg + " ]";
    }
    
}
