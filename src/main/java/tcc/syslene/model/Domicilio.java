/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc.syslene.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author iago
 */
@Entity
@Table(name = "domicilio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Domicilio.findAll", query = "SELECT d FROM Domicilio d"),
    @NamedQuery(name = "Domicilio.findByDmncodg", query = "SELECT d FROM Domicilio d WHERE d.dmncodg = :dmncodg"),
    @NamedQuery(name = "Domicilio.findByDmcmord", query = "SELECT d FROM Domicilio d WHERE d.dmcmord = :dmcmord"),
    @NamedQuery(name = "Domicilio.findByDmcendr", query = "SELECT d FROM Domicilio d WHERE d.dmcendr = :dmcendr"),
    @NamedQuery(name = "Domicilio.findByDmclat", query = "SELECT d FROM Domicilio d WHERE d.dmclat = :dmclat"),
    @NamedQuery(name = "Domicilio.findByDmclong", query = "SELECT d FROM Domicilio d WHERE d.dmclong = :dmclong"),
    @NamedQuery(name = "Domicilio.findByDmnhabt", query = "SELECT d FROM Domicilio d WHERE d.dmnhabt = :dmnhabt"),
    @NamedQuery(name = "Domicilio.findByDmlagua", query = "SELECT d FROM Domicilio d WHERE d.dmlagua = :dmlagua"),
    @NamedQuery(name = "Domicilio.findByDmlpoco", query = "SELECT d FROM Domicilio d WHERE d.dmlpoco = :dmlpoco"),
    @NamedQuery(name = "Domicilio.findByDmlcist", query = "SELECT d FROM Domicilio d WHERE d.dmlcist = :dmlcist"),
    @NamedQuery(name = "Domicilio.findByDmlresv", query = "SELECT d FROM Domicilio d WHERE d.dmlresv = :dmlresv"),
    @NamedQuery(name = "Domicilio.findByDmlres1", query = "SELECT d FROM Domicilio d WHERE d.dmlres1 = :dmlres1"),
    @NamedQuery(name = "Domicilio.findByDmlsani", query = "SELECT d FROM Domicilio d WHERE d.dmlsani = :dmlsani"),
    @NamedQuery(name = "Domicilio.findByDmlpia", query = "SELECT d FROM Domicilio d WHERE d.dmlpia = :dmlpia"),
    @NamedQuery(name = "Domicilio.findByDmltanq", query = "SELECT d FROM Domicilio d WHERE d.dmltanq = :dmltanq"),
    @NamedQuery(name = "Domicilio.findByDmlfilt", query = "SELECT d FROM Domicilio d WHERE d.dmlfilt = :dmlfilt"),
    @NamedQuery(name = "Domicilio.findByDmltan1", query = "SELECT d FROM Domicilio d WHERE d.dmltan1 = :dmltan1"),
    @NamedQuery(name = "Domicilio.findByDmlsumi", query = "SELECT d FROM Domicilio d WHERE d.dmlsumi = :dmlsumi"),
    @NamedQuery(name = "Domicilio.findByDmlvala", query = "SELECT d FROM Domicilio d WHERE d.dmlvala = :dmlvala"),
    @NamedQuery(name = "Domicilio.findByDmlreus", query = "SELECT d FROM Domicilio d WHERE d.dmlreus = :dmlreus"),
    @NamedQuery(name = "Domicilio.findByDmlesgt", query = "SELECT d FROM Domicilio d WHERE d.dmlesgt = :dmlesgt"),
    @NamedQuery(name = "Domicilio.findByDmlreci", query = "SELECT d FROM Domicilio d WHERE d.dmlreci = :dmlreci"),
    @NamedQuery(name = "Domicilio.findByDmdcadt", query = "SELECT d FROM Domicilio d WHERE d.dmdcadt = :dmdcadt"),
    @NamedQuery(name = "Domicilio.findByDmdaltr", query = "SELECT d FROM Domicilio d WHERE d.dmdaltr = :dmdaltr")})
public class Domicilio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "DMNCODG")
    private Integer dmncodg;
    @Column(name = "DMCMORD")
    private String dmcmord;
    @Column(name = "DMCENDR")
    private String dmcendr;
    @Column(name = "DMCLAT")
    private String dmclat;
    @Column(name = "DMCLONG")
    private String dmclong;
    @Column(name = "DMNHABT")
    private Integer dmnhabt;
    @Column(name = "DMLLEVT", columnDefinition = "BIT", length = 1)
    private Boolean dmllevt;
    @Column(name = "DMLAGUA", columnDefinition = "BIT", length = 1)
    private Boolean dmlagua;
    @Column(name = "DMLPOCO", columnDefinition = "BIT", length = 1)
    private Boolean dmlpoco;
    @Column(name = "DMLCIST", columnDefinition = "BIT", length = 1)
    private Boolean dmlcist;
    @Column(name = "DMLRESV", columnDefinition = "BIT", length = 1)
    private Boolean dmlresv;
    @Column(name = "DMLRES1", columnDefinition = "BIT", length = 1)
    private Boolean dmlres1;
    @Column(name = "DMLSANI", columnDefinition = "BIT", length = 1)
    private Boolean dmlsani;
    @Column(name = "DMLPIA", columnDefinition = "BIT", length = 1)
    private Boolean dmlpia;
    @Column(name = "DMLTANQ", columnDefinition = "BIT", length = 1)
    private Boolean dmltanq;
    @Column(name = "DMLFILT", columnDefinition = "BIT", length = 1)
    private Boolean dmlfilt;
    @Column(name = "DMLTAN1", columnDefinition = "BIT", length = 1)
    private Boolean dmltan1;
    @Column(name = "DMLSUMI", columnDefinition = "BIT", length = 1)
    private Boolean dmlsumi;
    @Column(name = "DMLVALA", columnDefinition = "BIT", length = 1)
    private Boolean dmlvala;
    @Column(name = "DMLREUS", columnDefinition = "BIT", length = 1)
    private Boolean dmlreus;
    @Column(name = "DMLESGT", columnDefinition = "BIT", length = 1)
    private Boolean dmlesgt;
    @Column(name = "DMLRECI", columnDefinition = "BIT", length = 1)
    private Boolean dmlreci;
    @Column(name = "DMDLEVT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dmdlevt;
    @Column(name = "DMDCADT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dmdcadt;
    @Column(name = "DMDALTR")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dmdaltr;
    @JoinColumn(name = "DMNCGLC", referencedColumnName = "LCNCODG")
    @ManyToOne
    private Localidade dmncglc;
    @JoinColumn(name = "DMNCGUS", referencedColumnName = "usncodg")
    @ManyToOne
    private Usuario dmncgus;

    public Domicilio() {
    	dmncgus = new Usuario();
    	dmncglc = new Localidade();
    }

    public Domicilio(Integer dmncodg) {
        this.dmncodg = dmncodg;
        dmncgus = new Usuario();
    	dmncglc = new Localidade();
    }

    public Boolean getDmllevt() {
		return dmllevt;
	}

	public void setDmllevt(Boolean dmllevt) {
		this.dmllevt = dmllevt;
	}

	public Integer getDmncodg() {
        return dmncodg;
    }

    public void setDmncodg(Integer dmncodg) {
        this.dmncodg = dmncodg;
    }

    public String getDmcmord() {
        return dmcmord;
    }

    public void setDmcmord(String dmcmord) {
        this.dmcmord = dmcmord;
    }

    public String getDmcendr() {
        return dmcendr;
    }

    public void setDmcendr(String dmcendr) {
        this.dmcendr = dmcendr;
    }

    public String getDmclat() {
        return dmclat;
    }

    public void setDmclat(String dmclat) {
        this.dmclat = dmclat;
    }

    public String getDmclong() {
        return dmclong;
    }

    public void setDmclong(String dmclong) {
        this.dmclong = dmclong;
    }

    public Integer getDmnhabt() {
        return dmnhabt;
    }

    public void setDmnhabt(Integer dmnhabt) {
        this.dmnhabt = dmnhabt;
    }

    public Boolean getDmlagua() {
        return dmlagua;
    }

    public void setDmlagua(Boolean dmlagua) {
        this.dmlagua = dmlagua;
    }

    public Boolean getDmlpoco() {
        return dmlpoco;
    }

    public void setDmlpoco(Boolean dmlpoco) {
        this.dmlpoco = dmlpoco;
    }

    public Boolean getDmlcist() {
        return dmlcist;
    }

    public void setDmlcist(Boolean dmlcist) {
        this.dmlcist = dmlcist;
    }

    public Boolean getDmlresv() {
        return dmlresv;
    }

    public void setDmlresv(Boolean dmlresv) {
        this.dmlresv = dmlresv;
    }

    public Boolean getDmlres1() {
        return dmlres1;
    }

    public void setDmlres1(Boolean dmlres1) {
        this.dmlres1 = dmlres1;
    }

    public Boolean getDmlsani() {
        return dmlsani;
    }

    public void setDmlsani(Boolean dmlsani) {
        this.dmlsani = dmlsani;
    }
    
    public Date getDmdlevt() {
		return dmdlevt;
	}

	public void setDmdlevt(Date dmdlevt) {
		this.dmdlevt = dmdlevt;
	}

	public Boolean getDmlpia() {
        return dmlpia;
    }

    public void setDmlpia(Boolean dmlpia) {
        this.dmlpia = dmlpia;
    }

    public Boolean getDmltanq() {
        return dmltanq;
    }

    public void setDmltanq(Boolean dmltanq) {
        this.dmltanq = dmltanq;
    }

    public Boolean getDmlfilt() {
        return dmlfilt;
    }

    public void setDmlfilt(Boolean dmlfilt) {
        this.dmlfilt = dmlfilt;
    }

    public Boolean getDmltan1() {
        return dmltan1;
    }

    public void setDmltan1(Boolean dmltan1) {
        this.dmltan1 = dmltan1;
    }

    public Boolean getDmlsumi() {
        return dmlsumi;
    }

    public void setDmlsumi(Boolean dmlsumi) {
        this.dmlsumi = dmlsumi;
    }

    public Boolean getDmlvala() {
        return dmlvala;
    }

    public void setDmlvala(Boolean dmlvala) {
        this.dmlvala = dmlvala;
    }

    public Boolean getDmlreus() {
        return dmlreus;
    }

    public void setDmlreus(Boolean dmlreus) {
        this.dmlreus = dmlreus;
    }

    public Boolean getDmlesgt() {
        return dmlesgt;
    }

    public void setDmlesgt(Boolean dmlesgt) {
        this.dmlesgt = dmlesgt;
    }

    public Boolean getDmlreci() {
        return dmlreci;
    }

    public void setDmlreci(Boolean dmlreci) {
        this.dmlreci = dmlreci;
    }

    public Date getDmdcadt() {
        return dmdcadt;
    }

    public void setDmdcadt(Date dmdcadt) {
        this.dmdcadt = dmdcadt;
    }

    public Date getDmdaltr() {
        return dmdaltr;
    }

    public void setDmdaltr(Date dmdaltr) {
        this.dmdaltr = dmdaltr;
    }

    public Localidade getDmncglc() {
        return dmncglc;
    }

    public void setDmncglc(Localidade dmncglc) {
        this.dmncglc = dmncglc;
    }

    public Usuario getDmncgus() {
        return dmncgus;
    }

    public void setDmncgus(Usuario dmncgus) {
        this.dmncgus = dmncgus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dmncodg != null ? dmncodg.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Domicilio)) {
            return false;
        }
        Domicilio other = (Domicilio) object;
        if ((this.dmncodg == null && other.dmncodg != null) || (this.dmncodg != null && !this.dmncodg.equals(other.dmncodg))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "syslene.Domicilio[ dmncodg=" + dmncodg + " ]";
    }
    
}
