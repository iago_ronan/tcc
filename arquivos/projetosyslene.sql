-- phpMyAdmin SQL Dump
-- version 4.0.10.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.12.109.130:3306
-- Tempo de Geração: 03/01/2017 às 07:02
-- Versão do servidor: 5.5.52
-- Versão do PHP: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `projetosyslene`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `domicilio`
--

CREATE TABLE IF NOT EXISTS `domicilio` (
  `DMNCODG` int(7) NOT NULL AUTO_INCREMENT,
  `DMNCGLC` int(7) NOT NULL,
  `DMNCGUS` int(7) NOT NULL,
  `DMCMORD` varchar(65) DEFAULT NULL,
  `DMCENDR` varchar(80) NOT NULL,
  `DMCLAT` varchar(20) DEFAULT NULL,
  `DMCLONG` varchar(20) DEFAULT NULL,
  `DMNHABT` int(2) NOT NULL,
  `DMLLEVT` tinyint(1) NOT NULL,
  `DMLAGUA` tinyint(1) NOT NULL,
  `DMLPOCO` tinyint(1) NOT NULL,
  `DMLCIST` tinyint(1) NOT NULL,
  `DMLRESV` tinyint(1) NOT NULL,
  `DMLRES1` tinyint(1) NOT NULL,
  `DMLSANI` tinyint(1) NOT NULL,
  `DMLPIA` tinyint(1) NOT NULL,
  `DMLTANQ` tinyint(1) NOT NULL,
  `DMLFILT` tinyint(1) NOT NULL,
  `DMLTAN1` tinyint(1) NOT NULL,
  `DMLSUMI` tinyint(1) NOT NULL,
  `DMLVALA` tinyint(1) NOT NULL,
  `DMLREUS` tinyint(1) NOT NULL,
  `DMLESGT` tinyint(1) NOT NULL,
  `DMLRECI` tinyint(1) NOT NULL,
  `DMDLEVT` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DMDCADT` timestamp NULL DEFAULT NULL,
  `DMDALTR` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`DMNCODG`),
  KEY `FKDMLC_idx` (`DMNCGLC`),
  KEY `FKDMUS_idx` (`DMNCGUS`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Fazendo dump de dados para tabela `domicilio`
--

INSERT INTO `domicilio` (`DMNCODG`, `DMNCGLC`, `DMNCGUS`, `DMCMORD`, `DMCENDR`, `DMCLAT`, `DMCLONG`, `DMNHABT`, `DMLLEVT`, `DMLAGUA`, `DMLPOCO`, `DMLCIST`, `DMLRESV`, `DMLRES1`, `DMLSANI`, `DMLPIA`, `DMLTANQ`, `DMLFILT`, `DMLTAN1`, `DMLSUMI`, `DMLVALA`, `DMLREUS`, `DMLESGT`, `DMLRECI`, `DMDLEVT`, `DMDCADT`, `DMDALTR`) VALUES
(1, 48, 1, 'Iago', 'rua 01, n 50', '-9.4390763', '-40.4885438', 2, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', NULL, NULL),
(19, 47, 2, 'João', 'Geolocation 2', '-9.4536712', '-40.5114423', 2, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-10-01 16:41:05', NULL),
(28, 57, 2, 'Marcelo', 'Geolocation 3', '-9.4306598', '-40.5012663', 2, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, '0000-00-00 00:00:00', '2016-10-01 21:53:12', NULL),
(29, 35, 2, 'valclecia alves varjao', 'travessa epitacio pessoa', '-9.444773', '-40.4964426', 2, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, '0000-00-00 00:00:00', '2016-10-03 01:23:25', NULL),
(36, 38, 3, 'outro teste', 'teste', '-9.430428', '-40.4949054', 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', '2016-10-05 19:19:24', NULL),
(37, 31, 2, 'artur', 'rua 01', '-8.87421701', '-40.4949054', 2, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', '2016-10-05 20:24:26', NULL),
(38, 3, 2, 'teste', 'teste', NULL, NULL, 2, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-10-14 23:55:16', NULL),
(39, 35, 2, 'João Duarte Varjão', 'Travessa Epitácio Pessoa, n 89', NULL, NULL, 2, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, '0000-00-00 00:00:00', '2016-10-15 16:15:35', NULL),
(40, 35, 2, 'joa duarte varjao', 'travessa epitacio pessoa', '-9.4435024', '-40.4972939', 2, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, '0000-00-00 00:00:00', '2016-10-15 16:25:50', NULL),
(41, 35, 2, 'marcelo', 'teste geo', '-9.4434739', '-40.4974078', 2, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, '0000-00-00 00:00:00', '2016-10-15 19:37:57', NULL),
(43, 5, 2, 'teste', 'rua 09', '-9.443434480545855', '-40.49719107070348', 2, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-10-15 22:19:10', NULL),
(44, 3, 2, 'Iago Ronan Vieira Santos', 'árvore', '-9.443399180499716', '-40.49704451186618', 2, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-10-15 22:19:10', NULL),
(45, 4, 2, 'jhon', 'uhu', '-9.443411273720443', '-40.497282279537', 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-10-17 17:52:05', NULL),
(46, 15, 2, 'qwe', 'teste', '-9.39628727427464', '-40.47871612271852', 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-10-17 17:52:05', NULL),
(47, 9, 4, 'teste', 'Teste', '-9.39609715', '-40.47872224', 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-10-17 18:10:59', NULL),
(48, 7, 2, 'facape', 'banheiro facape', '-9.395864755506778', '-40.47911054451622', 2, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-10-17 22:55:15', NULL),
(49, 4, 2, 'ford fiesta', 'meu carro estacionado', '-9.396393000980984', '-40.480033963801574', 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-10-18 00:03:41', NULL),
(50, 5, 2, 'ghjfg', 'fggkfhggh', '-9.396111777776055', '-40.478641433883105', 2, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-10-18 00:03:41', NULL),
(51, 2, 2, 'Helio', 'Rua 01', '-9.39610906019191', '-40.4787175608622', 2, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-10-18 00:23:53', NULL),
(52, 2, 2, 'Maria', 'Rua 01', '-9.396107162440655', '-40.47874201530979', 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-10-18 00:23:53', NULL),
(53, 7, 2, 'teste', 'lab 06', '-9.395506488225664', '-40.47870046593572', 2, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, '0000-00-00 00:00:00', '2016-10-21 23:57:06', NULL),
(54, 3, 2, '', 'jjk', '-9.443575502650761', '-40.49698347587033', 2, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-11-16 19:23:51', NULL),
(55, 7, 2, 'jonatas', 'rua 12345', '-9.396074865784048', '-40.47877177220989', 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-11-16 19:23:51', NULL),
(56, 1, 2, '', 'hgfdrhhg', '-9.44333449228688', '-40.497299199732716', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-12-06 12:36:00', NULL),
(57, 6, 2, '', 'rua 0', '-9.443402549897355', '-40.49748680146494', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-12-06 16:51:06', NULL),
(58, 8, 2, '', 'rua 00', '-9.44345070002348', '-40.497155909141036', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2016-12-06 16:51:06', NULL),
(59, 1, 2, '', 'oklhsjxjdjdk', '-9.443410996711847', '-40.4973731535866', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-12-11 00:52:15', '2016-12-10 03:34:07', NULL),
(60, 2, 2, 'teste', 'ikhhgh', '-9.443318954232288', '-40.497216288870206', 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, '2016-12-11 01:02:33', '2016-12-11 01:02:33', NULL),
(61, 2, 2, '', 'okjabje', '-9.396098926767369', '-40.47875355236627', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-12-12 18:31:47', '2016-12-12 18:31:47', NULL),
(63, 2, 2, '', '1i', '-9.443539793521463', '-40.49705460217396', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-12-13 10:50:14', '2016-12-13 10:50:14', NULL),
(64, 2, 2, 'ok', 'coleta de coordenadas está pronto!', '-9.443537114731772', '-40.49722277426511', 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, '2016-12-13 10:50:14', '2016-12-13 10:50:14', NULL),
(65, 1, 2, 'João Lucas', 'Travessa Epitácio Pessoa, N°153, Alto da aliança', '-9.443671214784976', '-40.49731022650808', 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-12-13 18:06:26', '2016-12-13 18:06:26', NULL),
(66, 6, 2, 'gfy', 'gfui', '-9.396103952368739', '-40.478718227224284', 2, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, '2016-12-13 18:06:26', '2016-12-13 18:06:26', NULL),
(67, 2, 2, '', 'fcc', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-12-13 23:07:02', '2016-12-13 23:07:02', NULL),
(68, 1, 2, 'Williaam', 'rua 4', '-9.39619495715826', '-40.47880894654062', 8, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, '2016-12-13 23:07:02', '2016-12-13 23:07:02', NULL),
(69, 1, 2, 'José', 'rua 4', '-9.396118973674112', '-40.478701305042776', 2, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, '2016-12-13 23:07:02', '2016-12-13 23:07:02', NULL),
(70, 4, 2, '', 'end1', '-9.39610963831008', '-40.47879408501346', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-12-13 23:07:02', '2016-12-13 23:07:02', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `localidade`
--

CREATE TABLE IF NOT EXISTS `localidade` (
  `LCNCODG` int(7) NOT NULL AUTO_INCREMENT,
  `LCCDESC` varchar(60) NOT NULL,
  `LCLAGUA` tinyint(1) NOT NULL,
  `LCLESGT` tinyint(1) NOT NULL,
  `LCLCOLT` tinyint(1) NOT NULL,
  `LCDALTR` timestamp NULL DEFAULT NULL,
  `LCNCGUS` int(7) DEFAULT NULL,
  PRIMARY KEY (`LCNCODG`),
  KEY `LCNCGUS` (`LCNCGUS`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=119 ;

--
-- Fazendo dump de dados para tabela `localidade`
--

INSERT INTO `localidade` (`LCNCODG`, `LCCDESC`, `LCLAGUA`, `LCLESGT`, `LCLCOLT`, `LCDALTR`, `LCNCGUS`) VALUES
(1, 'Abóbora', 1, 1, 0, '2016-12-13 23:07:02', NULL),
(2, 'Aboboras', 1, 1, 0, '2016-12-13 18:06:26', NULL),
(3, 'Acude da Rancharia', 0, 0, 0, NULL, NULL),
(4, 'Alfavaca', 0, 0, 0, NULL, NULL),
(5, 'Angico', 0, 0, 0, NULL, NULL),
(6, 'Arapua Novo', 0, 0, 0, NULL, NULL),
(7, 'Assentamento Fonte Viva', 0, 0, 0, NULL, NULL),
(8, 'Assentamento Sao Francisco', 0, 0, 0, NULL, NULL),
(9, 'Barauna', 0, 0, 0, NULL, NULL),
(10, 'Barrinha do Cambao', 0, 0, 0, NULL, NULL),
(11, 'Boa Vista', 0, 0, 0, NULL, NULL),
(12, 'Boqueirao', 0, 0, 0, NULL, NULL),
(13, 'Campo dos Cavalos', 0, 0, 0, NULL, NULL),
(14, 'Campos', 0, 0, 0, NULL, NULL),
(15, 'Capim da Raiz', 0, 0, 0, NULL, NULL),
(16, 'Carnaíba do Sertão', 0, 0, 0, NULL, NULL),
(17, 'Carneiro', 0, 0, 0, NULL, NULL),
(18, 'Comunidade Agricola de Juazeiro', 0, 0, 0, NULL, NULL),
(19, 'Conchas', 0, 0, 0, NULL, NULL),
(20, 'Curral Novo', 0, 0, 0, NULL, NULL),
(21, 'E E F Sweet Fruit', 0, 0, 0, NULL, NULL),
(22, 'Fazenda Fuka', 0, 0, 0, NULL, NULL),
(23, 'Fazenda Global Lote C', 0, 0, 0, NULL, NULL),
(24, 'Fazenda Sasaki', 0, 0, 0, NULL, NULL),
(25, 'Gangorra I', 0, 0, 0, NULL, NULL),
(26, 'Gangorra II', 0, 0, 0, NULL, NULL),
(27, 'Goiabeira', 0, 0, 0, NULL, NULL),
(28, 'Goiabeira II', 0, 0, 0, NULL, NULL),
(29, 'Guanhaes', 0, 0, 0, NULL, NULL),
(30, 'Horto', 0, 0, 0, NULL, NULL),
(31, 'Itamotinga', 0, 0, 0, NULL, NULL),
(32, 'Itaparica', 0, 0, 0, NULL, NULL),
(33, 'Jardim Primavera', 0, 0, 0, NULL, NULL),
(34, 'Jatoba', 0, 0, 0, NULL, NULL),
(35, 'Bairro Alto da Aliança', 0, 0, 0, NULL, NULL),
(36, 'Bairro Alto da Maravilha', 0, 0, 0, NULL, NULL),
(37, 'Bairro Alto do Alencar', 0, 0, 0, NULL, NULL),
(38, 'Bairro Alto do Cruzeiro', 0, 0, 0, NULL, NULL),
(39, 'Bairro Antonio Guilhermino', 0, 0, 0, NULL, NULL),
(40, 'Bairro Argemiro', 0, 0, 0, NULL, NULL),
(41, 'Bairro Cajueiro', 0, 0, 0, NULL, NULL),
(42, 'Bairro Castelo Branco', 0, 0, 0, NULL, NULL),
(43, 'Bairro Centenário', 0, 0, 0, NULL, NULL),
(44, 'Bairro Centro', 0, 0, 0, NULL, NULL),
(45, 'Bairro Coréia', 0, 0, 0, NULL, NULL),
(46, 'Bairro Country Club', 0, 0, 0, NULL, NULL),
(47, 'Bairro Dom José Rodrigues', 0, 0, 0, NULL, NULL),
(48, 'Bairro Dom Tomaz', 0, 0, 0, NULL, NULL),
(49, 'Bairro Expedito de Almeida Nascimento', 0, 0, 0, NULL, NULL),
(50, 'Bairro Itaberaba', 0, 0, 0, NULL, NULL),
(51, 'Bairro Jardim Flórida', 0, 0, 0, NULL, NULL),
(52, 'Bairro Jardim Novo Encontro', 0, 0, 0, NULL, NULL),
(53, 'Bairro Jardim São Paulo', 0, 0, 0, NULL, NULL),
(54, 'Bairro Jardim Universitário', 0, 0, 0, NULL, NULL),
(55, 'Bairro Jardim Vitória', 0, 0, 0, NULL, NULL),
(56, 'Bairro João Paulo II', 0, 0, 0, NULL, NULL),
(57, 'Bairro João XXIII', 0, 0, 0, NULL, NULL),
(58, 'Bairro Juazeiro Velho', 0, 0, 0, NULL, NULL),
(59, 'Bairro Lomanto Júnior', 0, 0, 0, NULL, NULL),
(60, 'Bairro Malhada da Areia', 0, 0, 0, NULL, NULL),
(61, 'Bairro Maringá', 0, 0, 0, NULL, NULL),
(62, 'Bairro Matatu', 0, 0, 0, NULL, NULL),
(63, 'Bairro Nossa Senhora da Penha', 0, 0, 0, NULL, NULL),
(64, 'Bairro Padre Vicente', 0, 0, 0, NULL, NULL),
(65, 'Bairro Palmares', 0, 0, 0, NULL, NULL),
(66, 'Bairro Parque Centenário', 0, 0, 0, NULL, NULL),
(67, 'Bairro Pedra do Lord', 0, 0, 0, NULL, NULL),
(68, 'Bairro Pedro Raimundo', 0, 0, 0, NULL, NULL),
(69, 'Bairro Piranga', 0, 0, 0, NULL, NULL),
(70, 'Bairro Piranga II', 0, 0, 0, NULL, NULL),
(71, 'Bairro Quide', 0, 0, 0, NULL, NULL),
(72, 'Bairro Santa Maria Goretti', 0, 0, 0, NULL, NULL),
(73, 'Bairro Santo Antônio', 0, 0, 0, NULL, NULL),
(74, 'Bairro São Geraldo', 0, 0, 0, NULL, NULL),
(75, 'Bairro Tabuleiro', 0, 0, 0, NULL, NULL),
(76, 'Bairro Tancredo Neves', 0, 0, 0, NULL, NULL),
(77, 'Bairro Vila Tiradentes', 0, 0, 0, NULL, NULL),
(78, 'Junco', 0, 0, 0, NULL, NULL),
(79, 'Juremal', 0, 0, 0, NULL, NULL),
(80, 'Lagoa da Pedra', 0, 0, 0, NULL, NULL),
(81, 'Lagoa do Salitre', 0, 0, 0, NULL, NULL),
(82, 'Lajinha', 0, 0, 0, NULL, NULL),
(83, 'Mandacaru II', 0, 0, 0, NULL, NULL),
(84, 'Manga I', 0, 0, 0, NULL, NULL),
(85, 'Manga II', 0, 0, 0, NULL, NULL),
(86, 'Manicoba', 0, 0, 0, NULL, NULL),
(87, 'Marruá', 0, 0, 0, NULL, NULL),
(88, 'Massaroca', 0, 0, 0, NULL, NULL),
(89, 'Nucleo  Projeto Curaca ', 0, 0, 0, NULL, NULL),
(90, 'Nucleo II Projeto Manicoba', 0, 0, 0, NULL, NULL),
(91, 'Pau Preto', 0, 0, 0, NULL, NULL),
(92, 'Pinhões', 0, 0, 0, NULL, NULL),
(93, 'Pocoes', 0, 0, 0, NULL, NULL),
(94, 'Pontal', 0, 0, 0, NULL, NULL),
(95, 'Porto da Pedra', 0, 0, 0, NULL, NULL),
(96, 'Projeto Curaca', 0, 0, 0, NULL, NULL),
(97, 'Projeto Curaca Nucleo III', 0, 0, 0, NULL, NULL),
(98, 'Projeto Curaca Nucleo Iv', 0, 0, 0, NULL, NULL),
(99, 'Projeto Tourao Mandacaru II', 0, 0, 0, NULL, NULL),
(100, 'R do Sol', 0, 0, 0, NULL, NULL),
(101, 'Riacho da Massaroca', 0, 0, 0, NULL, NULL),
(102, 'Rodeadouro', 0, 0, 0, NULL, NULL),
(103, 'Sabia II', 0, 0, 0, NULL, NULL),
(104, 'Saco do Meio', 0, 0, 0, NULL, NULL),
(105, 'Sao Francisco', 0, 0, 0, NULL, NULL),
(106, 'Sao Goncalo do Salitre', 0, 0, 0, NULL, NULL),
(107, 'Sitio Paraiso', 0, 0, 0, NULL, NULL),
(108, 'Sitio Sol Nascente', 0, 0, 0, NULL, NULL),
(109, 'Sitio Uzumaki Cd', 0, 0, 0, NULL, NULL),
(110, 'Sobradinho', 0, 0, 0, NULL, NULL),
(111, 'Tapera', 0, 0, 0, NULL, NULL),
(112, 'Tourao', 0, 0, 0, NULL, NULL),
(113, 'Umbuzeiro do Salitre', 0, 0, 0, NULL, NULL),
(114, 'Usina Mandacaru Nucleo II', 0, 0, 0, NULL, NULL),
(115, 'Usina Mandacaru Nucleo V', 0, 0, 0, NULL, NULL),
(116, 'Vila Juca VIana ', 0, 0, 0, NULL, NULL),
(117, 'Vila Juca VIana ', 0, 0, 0, NULL, NULL),
(118, 'Vila Kipa', 0, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `parametro`
--

CREATE TABLE IF NOT EXISTS `parametro` (
  `PANCODG` int(2) NOT NULL AUTO_INCREMENT,
  `PACUF` varchar(2) NOT NULL,
  `PACMUNI` varchar(60) NOT NULL,
  PRIMARY KEY (`PANCODG`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Fazendo dump de dados para tabela `parametro`
--

INSERT INTO `parametro` (`PANCODG`, `PACUF`, `PACMUNI`) VALUES
(1, 'BA', 'JUAZEIRO');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tipousuario`
--

CREATE TABLE IF NOT EXISTS `tipousuario` (
  `tuncodg` int(7) NOT NULL AUTO_INCREMENT,
  `tucdesc` varchar(45) NOT NULL,
  PRIMARY KEY (`tuncodg`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Fazendo dump de dados para tabela `tipousuario`
--

INSERT INTO `tipousuario` (`tuncodg`, `tucdesc`) VALUES
(1, 'Administrador'),
(2, 'Técnico');

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `usncodg` int(7) NOT NULL AUTO_INCREMENT,
  `usncgtu` int(7) NOT NULL,
  `uscdesc` varchar(60) NOT NULL,
  `uscmail` varchar(60) DEFAULT NULL,
  `usclogn` varchar(45) NOT NULL,
  `uscpasw` varchar(45) NOT NULL,
  `usciddp` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`usncodg`),
  KEY `FKUSTU_idx` (`usncgtu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Fazendo dump de dados para tabela `usuario`
--

INSERT INTO `usuario` (`usncodg`, `usncgtu`, `uscdesc`, `uscmail`, `usclogn`, `uscpasw`, `usciddp`) VALUES
(1, 1, 'Administrador', 'admin@admin.com', 'admin', 'admin', '9999'),
(2, 1, 'Iago Ronan', 'iago.santos@facape.br', 'iago', '123', 'fa181fd1b11fb48f'),
(3, 2, 'Wanderlúcio', '', 'lucio', 'lucio', 'a801107d453a1fd8'),
(4, 2, 'Artur', '', 'artur', 'artur', 'f4e2f9e99dfcd0ed'),
(5, 1, 'Carlos', '', 'carlos', 'pp17', '');

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `domicilio`
--
ALTER TABLE `domicilio`
  ADD CONSTRAINT `FKDMLC` FOREIGN KEY (`DMNCGLC`) REFERENCES `localidade` (`LCNCODG`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FKDMUS` FOREIGN KEY (`DMNCGUS`) REFERENCES `usuario` (`usncodg`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `localidade`
--
ALTER TABLE `localidade`
  ADD CONSTRAINT `localidade_ibfk_1` FOREIGN KEY (`LCNCGUS`) REFERENCES `usuario` (`usncodg`);

--
-- Restrições para tabelas `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `FKUSTU` FOREIGN KEY (`usncgtu`) REFERENCES `tipousuario` (`tuncodg`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
